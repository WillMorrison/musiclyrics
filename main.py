import requests
import xml.etree.ElementTree as ET


def get_lyrics_of_song(artist, title):
    api_url = "http://api.chartlyrics.com/apiv1.asmx/"
    response = requests.get(f'{api_url}/SearchLyricDirect?artist={artist}&song={title}')

    if response.status_code == 500:
        return "error"

    tree = ET.fromstring(response.content)

    return tree[9].text

